# Dicionário de Expressões 0.1
> Plugin para LibreOffice que traduz expressões em textos para LIBRAS (Lingua Brasileira de Sinais) quando elas são selecionadas


O Dicionário de Expressões utiliza a API UNO (Universal Network Objects) do LibreOffice através do Python 3.6, ou seja
através da "ponte" PyUNO. A ponte PyUNO é responsável pela conexão com o LibreOffice em tempo de execução (basta que o LibreOffice
tenha sido iniciado através do "modo escuta"), capturando trechos de texto selecionados. 

## Breve explicação sobre como o 'Dicionário de Expressões 0.1' funciona

1. Conexão com o LibreOffice (Indentifica o texto selecionado no LibreOffice):
    * A função identificaTextoSelecionado() em Funcoes/LabIS_UFRJ_Ferramentas.py é responsável pela ponte PyUNO, capturando textos selecionados pelo usuário. 
    
2. Busca no banco de dados:
    * O Dicionário de Expressões funciona com um (até o momento) pequeno banco de dados no formato .JSON (escolhido devido a atual complexidade da aplicação e facilidade de manipulação). O banco de dados .JSON deve estar na mesma pasta que os .GIFS utilizados para a exibição dos sinais. O arquivo .JSON do banco de dados está localizado em BancoDeDados/dicionarioDeExpressoes.json
    * O banco de dados foi localmente implementado em Python, e tem todas as ferramentas necessárias para o funcionamento da aplicação. O código pode ser verificado em Funcoes/LabIS_UFRJ_BancoDeDados.py. Um método interessante do banco de dados é o adicionaNoBD_adicionaisNaPasta(self), que ao iniciar a aplicação do Dicionário de expressões, é chamado para verificar novos .GIFS na pasta de GIFS, e adicionar no arquivo .JSON as expressões que ainda não constam, permitindo que novos sinais sejam adicionados na aplicação apenas colocando-os na pasta de sinais.
   
3. Exibição dos sinais
    * Caso o sinal .GIF exista no banco de dados .JSON do Dicionário de Expressões, ele busca o sinal na pasta de .GIFS e exibe através da função exibeSinal(nomeDoSinal, enderecoDeExecucao) escrita em Funcoes/LabIS_UFRJ_Ferramentas.py, que usa o PySide2 (Qt)

![](header.png)

## Instalação

Linux (baseado no Debian):

``` 1. Instalação de dependências
        * sudo apt install libreoffice-dev
        * sudo apt install git
        * sudo apt install python3-uno
        * sudo apt install python3-pip
        * pip3 install PySide2
        
    2. Clone do repositório
        * git clone https://gitlab.com/LabIS-UFRJ/LIBRASOffice/dicionario-de-expressoes-plugin.git
        * Abra o terminal na pasta onde o arquivo foi clonado e digite: soffice --accept="socket,host=localhost,port=2002;urp;StarOffice.ServiceManager"
        * Agora digite python3 DicionarioDeExpressoes.py
        * Pronto. Agora, basta selecionar expressões ou palavras em textos, e se o que foi selecionado estiver no banco de dados,
        um sinal será exibido. 
```

Windows:

```!Ainda não testado! 
```

## Exemplo de uso

Após todas as etapas de instalação, você pode iniciar o LibreOffice em modo de escuta, abrir o LibreOffice Writer, selecionar um texto para uso, gravar os sinais para ele e colocá-los na pasta de sinais. Logo após, utilizar a aplicação em alguma aula (por exemplo)

## Etapas para desenvolvimento

As etapas para desenvolvimento podem ser vistas como as mesmas para a execução do Plugin (visto que ele ainda não é executado em modo stand-alone). Basta seguir as estapas da instalação e explorar o código :) 

```Nota: Verifique o Kanban e veja como você pode ajudar no projeto! Você também pode sugerir coisas para o Kanban! :)
```

## Como contribuir?

1. Fork (https://gitlab.com/LabIS-UFRJ/LIBRASOffice/dicionario-de-expressoes-plugin.git)
2. Crie uma branch para você não fazer alterações diretamente na master (`git checkout -b suaBranch`)
3. Commit suas alterações (`git commit -m 'Um comentário sobre o seu commit'`)
4. Envie suas alterações para a sua conta no GitLab (`git push origin suaBranch`)
5. Crie uma nova Pull Request

## Quem somos?

Laboratório de Informática e Sociedade (LabIS) -  Universidade Federal do Rio de Janeiro
> Av. Horácio Macedo 2030, Prédio do Centro de Tecnologia, Bloco H, sala H304/12 Cidade Universitária - CEP: 21941-914, Rio de Janeiro/RJ - Brasil

E-mail: labis@cos.ufrj.br

