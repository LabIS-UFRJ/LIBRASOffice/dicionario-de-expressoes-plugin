# -*- coding: utf-8 -*-
import uno
from time import sleep

from PySide2.QtWidgets import *
from PySide2.QtQuick import *
from PySide2.QtCore import *
import os


def identificaTextoSelecionado():
    try:
        sleep(0.3)
        contexto = uno.getComponentContext()
        resolver = contexto.ServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver",
                                                                 contexto)
        serviceManager = resolver.resolve("uno:socket,host=localhost,port=8100;urp;StarOffice.ServiceManager")
        currentLibreOffice = serviceManager.createInstanceWithContext( "com.sun.star.frame.Desktop",contexto)
        arquivo = currentLibreOffice.getCurrentComponent()
        textoSelecionado = arquivo.CurrentSelection
        textoSelecionado = textoSelecionado.getByIndex(0)
        textoSelecionado = textoSelecionado.String
        return textoSelecionado.lower().strip()
    except Exception as e:
        print(f'{e!s}: {e!r}')
    

def iniciaLibreOffice():
    os.system('soffice --accept="socket,host=localhost,port=2002;urp;StarOffice.ServiceManager"')
    

def exibeSinal(nomeDoSinal, enderecoDeExecucao):

    #Endereço da "Interface gráfica"
    endQml = ""
    if (os.name == "posix"):
        endQml = enderecoDeExecucao + "/Qml/sinal_gif_janela.qml"
    else:
        endQml = enderecoDeExecucao + "\\Qml\\sinal_gif_janela.qml"

    #Endereço da pasta com os sinais
    endSinal = ""
    if (os.name == "posix"):
        endSinal = enderecoDeExecucao + "/BancoDeDados/SINAIS/" + nomeDoSinal + ".gif"
    else:
        endSinal = enderecoDeExecucao + "\\BancoDeDados\\SINAIS\\" + nomeDoSinal + ".gif"

    app = QApplication([])
    view = QQuickView()
    contexto = view.rootContext()
    contexto.setContextProperty('librasGif', endSinal)
    view.setFlag(Qt.FramelessWindowHint)
    url = QUrl(endQml)
    view.setSource(url)
    view.show()
    tamanhoDaTela = app.desktop().screenGeometry()
    X = tamanhoDaTela.width() - 200
    Y = tamanhoDaTela.height() - 250

    view.setPosition(X, Y)

    app.exec_()
    
