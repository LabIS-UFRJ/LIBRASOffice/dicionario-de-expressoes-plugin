# -*- coding: utf-8 -*-
import json
from os import walk


class DicionarioDeExpressoes_BD:
    def __init__(self, endBD, endPastaComSinais):
        '''o argumento passado (endBD) tem que ser um endereço absoluto (ou um endereço local válido) de um
           arquivo .JSON com o nome dos sinais que existem'''
        self.__endBD = endBD
        self.__dicBD = {}
        self.__pastaComSinais = endPastaComSinais
        
        try:
            with open(self.__endBD) as jsonBD:
                self.__dicBD = json.load(jsonBD)
        except Exception as e:
            print(f'{e!s}: {e!r}')
            return None

    def getBD(self):
        return self.__dicBD

    def importBD(self):
        try:
            with open(self.__endBD) as jsonBD:
                self.__dicBD = json.load(jsonBD)
        except Exception as e:
            print(f'{e!s}: {e!r}')
            return None

    def adicionaNoBD(self, expressao):
        try:
            self.__dicBD[expressao] = 'True'
            with open(self.__endBD, 'w') as jsonBD:
                json.dump(self.__dicBD, jsonBD, indent=4, ensure_ascii=False)
            self.importBD()
        except Exception as e:
            print(f'{e!s}: {e!r}')
            return None

    def buscaNoBD(self, expressao):
        if(expressao in self.__dicBD.keys()):
            return True
        else:
            return False

    def atualizaBD(self):
        try:
            with open(self.__endBD, 'w') as jsonBD:
                json.dump(self.__dicBD, jsonBD, indent=4, ensure_ascii=False)
            self.importBD()
        except Exception as e:
            print(f'{e!s}: {e!r}')
            return None

    def adicionaNoBD_adicionaisNaPasta(self):
        path = self.__pastaComSinais
        gifs = []
        try:
            for r, d, f in walk(path):
                for gif in f:
                    if '.gif' in gif:
                        gifs.append(gif.replace(".gif", '').strip())
            for gif in gifs:
                if gif not in self.__dicBD.keys():
                    self.__dicBD[gif] = "True"
        except Exception as e:
            print(f'{e!s}: {e!r}')
            return None
        self.atualizaBD()
