import QtQuick 2.9
import QtQuick.Window 2.0
import QtQuick.Controls 2.0

Rectangle {
    width: 200
    height: 250
    AnimatedImage {
        id: animatedImage
        x: 0
        y: 0
        width: 200
        height: 250
        source: librasGif
    }

}
