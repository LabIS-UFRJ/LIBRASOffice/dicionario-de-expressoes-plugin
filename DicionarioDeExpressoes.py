# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, "./")
from Funcoes.LabIS_UFRJ_BancoDeDados import *
from Funcoes.LabIS_UFRJ_Ferramentas import *
from time import sleep
import os

from multiprocessing import Process

#Obtendo o endereço absoluto atual (por onde o dicionário de expressões está sendo executado
enderecoAtual = os.path.dirname(os.path.realpath(__file__))

#Obtendo o endereço do .JSON (banco de dados) e da pasta com sinais
endBD = ""
endPastaComSinais = ""
if (os.name == "posix"):
    endBD = enderecoAtual + "/BancoDeDados/dicionarioDeExpressoes.json"
    endPastaComSinais = enderecoAtual + "/BancoDeDados/SINAIS"
else:
    endBD = enderecoAtual + "\\BancoDeDados\\dicionarioDeExpressoes.json"
    endPastaComSinais = enderecoAtual + "\\BancoDeDados\\SINAIS"
    


DicBD = DicionarioDeExpressoes_BD(endBD, endPastaComSinais)
DicBD.adicionaNoBD_adicionaisNaPasta()

while(True):
    current_expr = identificaTextoSelecionado()
    #Se current_expr for None, significa que nao ha LibreOffice aberto, ou houveram problemas com a conexao
    if(current_expr != None):
        if(current_expr == ''):
            continue
        elif(DicBD.buscaNoBD(current_expr)):
            subProcesso = Process(target=exibeSinal, args=(current_expr, enderecoAtual))
            subProcesso.start()
            print("SINAL SELECIONADO: " + current_expr)
            while(current_expr == identificaTextoSelecionado()):
                sleep(0.2)
            subProcesso.terminate()
    else:
        break
        
